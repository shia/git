exec_cmd.c
---

	const char *git_extract_argv0_path(const char *argv0)
	提取argv0的路径,实参为argv[0].	

---
`git_extract_argv0_path`函数有两处值得称道的地方：  
1. `slash`指针，逆向确定文件分隔符(direction separator)的位置		
2. `xstrndup`函数,此函数定义于`wrapper.c`中，与一般的strndup不同之处在与发生错误时退出程序 

代码中定义了宏：`is_dir_sep(c) ((c) == '/' || (c) == '\\')`,该版本兼容windows。 

	const char *git_extract_argv0_path(const char *argv0)
	{
		const char *slash;

		if (!argv0 || !*argv0)
			return NULL;

		slash = argv0 + strlen(argv0);
		while (argv0 <= slash && !is_dir_sep(*slash)) 
			slash--;

		if (slash >= argv0) {
			argv0_path = xstrndup(argv0, slash - argv0);
			return slash + 1;
		}

		return argv0;
	}

argv[0]是当前正在执行的程序的名字，由宿主环境提供.由`exec()`的一族函数确定。
其内容可在Linux中可以通过查看`proc/<PID>/cmdline`得到.
